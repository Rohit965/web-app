FROM maven:3-alpine as build-stage
WORKDIR /tmp
COPY ./ /tmp
RUN cd /tmp && \
    mvn clean install -DskipTests





FROM nginx

COPY --from=build-stage /tmp/target/web-app-1.0.war /usr/share/nginx/html/